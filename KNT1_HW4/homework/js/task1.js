// Your code goes here
function getUserData(restrictions_min=NaN, message, correctly, restrictions_max=NaN){
	let data = Number(prompt(`${message}:`, `${correctly}`));
	if (data >= restrictions_min && !isNaN(data) && (isNaN(restrictions_max) || data < restrictions_max)) {
		return data;
	}
	alert('Invalid input data')
	return getUserData(restrictions_min, message, correctly, restrictions_max)
}
function calculationResult(initial_money, number_of_years, percentage) {
	let total_amount = initial_money, total_profit;
	let msg = ``
	for(let i = 1; i <= number_of_years; i++) {
		let temp = total_amount
		total_amount = total_amount + total_amount * (percentage / 100);
		total_profit = total_amount - initial_money;
		if(i > 1) {
			msg = msg + `\n${i} Year\n
					 Total profit:${total_profit}(previous profit + ${percentage}% from previous total amount (${temp}))
					 Total amount: ${total_amount} (initial amount + total profit)`
		}else {
			msg = msg + `\n${i} Year\n
					 Total profit: ${total_profit} (${percentage}% from initial amount)
					 Total amount: ${total_amount} (initial amount + total profit)`
		}
	}
	return `\nTotal profit: ${total_profit}\nTotal amount: ${total_amount}` + msg
}
function main(){
	let initial_money = getUserData(1000, 'Enter the initial amount of money', 1000);
	let number_of_years = getUserData(1, 'Enter the number of years', 1);
	while (!(number_of_years % 1 === 0)) {
		alert('Invalid input data')
        number_of_years = getUserData(1, 'Enter the number of years', 3);
	}
	let percentage = getUserData(0, 'Enter the percentage of a year', 10, 100)
	let result =
		'Initial amount: ${initial_money}\n' +
		'Number of years: ${number_of_years}\n' +
		'Percentage of year: ${percentage}\n\n' + calculationResult(initial_money, number_of_years, percentage)
    alert(result)
}
main()
