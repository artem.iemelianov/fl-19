// Your code goes here
let start_money = 100
function getUserNumber(range_max, life, count, max_prize) {
    let user_num = Number(prompt(`Choose a roulette pocket number from 0 to ${range_max}\n
                                   Attempts left: ${life}\n
                                   Total prize: ${count}$\n
                                   Possible prize on current attempt: ${max_prize}$`))
    if (!isNaN(parseFloat(user_num)) && isFinite(user_num) && user_num >= 0 && user_num <= 8) {
        return user_num
    }
    return getUserNumber(range_max, life, count, max_prize)
}
function play(life = 3, range_max = 8, count = 0, max_prize = start_money) {
    let num = Math.floor(Math.random() * range_max);
    let user_num;
    while (life !== 0 || life < 0) {
        max_prize = start_money
        if(life === 2) {
            max_prize = start_money / 2
        } else if (life === 1) {
            max_prize = start_money / 4
        }
        user_num = getUserNumber(range_max, life, count, max_prize)
        if(num !== user_num) {
            life = life - 1
        } else {
            count = count + max_prize
            if (confirm('Congratulation, you won! Your prize is: ${max_prize}$. Do you want to continue?') === true) {
                range_max = Math.floor(range_max + range_max / 2)
                start_money = start_money + 100
                max_prize = start_money
                life = 3
                return play(range_max, count, start_money)
            }
        }
    }
    if(confirm(`Thank you for your participation. Your prize is: ${count}$\nPlay again?`)) {
        start_money = 100
        return play()
    }
}
function startPoint() {
    if (confirm('Do you want to play a game?') === true) {
        play()
    } else {
        alert('You did not become a billionaire, but can.')
    }
}
startPoint()