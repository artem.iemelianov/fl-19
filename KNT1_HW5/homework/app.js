function reverseNumber(num) {
    let result = 0, counter = 0, isNegative = num < 0;
    let getRestDigit = (n) => n / 10 - n % 10 * 0.1;
    for (let i = Math.abs(num); i >= 1; i = getRestDigit(i)) {
        counter = i % 10;
        result = result * 10 + counter;
    }
    return isNegative ? -result : result;
}
function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}
function map(arr, func) {
    let resultArr = [];
    for (let i = 0; i < arr.length; i++) {
        resultArr.push(func(arr[i]));
    }
    return resultArr;
}
function filter(arr, func) {
    let resultArr = [];
    for (let i = 0; i < arr.length; i++) {
        if (func(arr[i])) {
            resultArr.push(arr[i]);
        }
    }
    return resultArr;
}
function getAdultAppleLovers(data) {
    let predicate = (item) =>
        item['age'] > 18 && item['favoriteFruit'] === 'apple'

    let adultAppleLovers = filter(data, predicate);
    return map(adultAppleLovers, (item) => item['name']);
}
function getKeys(obj) {
    let result = [];
    for (let i in obj) {
        result.push(i);
    }
    return result;
}
function getValues(obj) {
    let result = [];
    for (let i in obj) {
        result.push(obj[i]);
    }
    return result;
}

function showFormattedDate(dateObj) {
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let day = dateObj.getDate();
    let month = months[dateObj.getMonth()];
    let year = dateObj.getFullYear();
    return `It is ${day} of ${month}, ${year}`;
}
const data = [
    {
        '_id': '5b5e3168c6bf40f2c1235cd6',
        'index': 0,
        'age': 39,
        'eyeColor': 'green',
        'name': 'Stein',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e3168e328c0d72e4f27d8',
        'index': 1,
        'age': 38,
        'eyeColor': 'blue',
        'name': 'Cortez',
        'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
    }
]
console.log(reverseNumber(12345));
console.log(reverseNumber(-56789));
forEach([2,5,8], function(el) { 
    console.log(el) 
});
console.log(map([2, 5, 8], function(el) {
    return el + 3;
}));
console.log(map([1, 2, 3, 4, 5], function (el) {
    return el * 2;
}));
console.log(filter([2, 5, 1, 3, 8, 6], function(el) {
    return el > 3
}));
console.log(filter([1, 4, 6, 7, 8, 10], function(el) {
    return el % 2 === 0
}));
console.log(getAdultAppleLovers(data));
console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3}));
console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3}));
console.log(showFormattedDate(new Date('2018-08-27T01:10:00')));