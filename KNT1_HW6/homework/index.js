// Your code goes here
function isEquals(x1, x2) {
    return x1 === x2;
}
function isBigger(x1, x2) {
    return x1 > x2;
}
function storeNames() {
    let arr = []
    for (let i = 0; i < arguments.length; i++) {
        console.log(arguments[i])
        arr[i] = arguments[i]
    }
    return arr
}
function getDifference(x1, x2) {
    return x1 > x2 ? x1 - x2 : x2 - x1
}
function count(arr) {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        count += arr[i] < 0 ? 1 : 0
    }
    return count
}
function letterCount(str1, str2) {
    let count = 0;
    for(let i = 0; i < str1.length; i++) {
        if (str1[i] === str2) {
            count += 1
        }
    }
    return count
}
function countPoints(arr) {
    let count = 0;
    for (let i = 0; i < arr.length; i++) {
        let temp = arr[i].replace(':', ' ').split(/\s+/)
        count += Number(temp[0]) > Number(temp[1]) ? 3 : 0
        if (Number(temp[0]) === Number(temp[1])) { 
            count += 1 
        }
    }
    return count
}
console.log(isEquals(3,3))
console.log(isBigger(5,-1))
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'))
console.log(getDifference(5,3))
console.log(getDifference(5,8))
console.log(count([4, 3, 2, 9]))
console.log(count([0, -3, 5, 7]))
console.log(letterCount('Marry', 'r'))
console.log(letterCount('Barny', 'y'))
console.log(letterCount('', 'z')) 
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']))